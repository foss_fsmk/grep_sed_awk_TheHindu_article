Managing/Tracking your finances from the command line

In this article I write about three command line utilities grep sed awk
https://www.kaggle.com/datasets/trinaghosh346/personal-income-expenditure

grep -lr search_pattern folder
grep -lr awk Article2/

grep

grep search_pattern file_list
List the transactions of American Express Credit Card
grep  "American Express Credit Card" Expenses.csv


Count number of Pharmacy related transactions
grep -c Pharmacy Expenses.csv 

grep cannot make changes

sed 

Adding Empty lines between text
sed -e 'i\ ' Expenses2.csv
sed -e 'i\\n' Expenses2.csv

Emphasize lines having a particular word

sed -e '/Food/a================' Expenses2.csv

Double Highlighting

sed -e '/Food/a================' -e '/Food/i================' Expenses2.csv


Awk

Formatting the output

awk -F"," 'NR > 1 {printf("%10s\t%.3s  %16.2f   %-10.10s\t %-30s\n", $1, $2, $3, $4, $6)}' Expenses.csv  


Fetching types of categories
awk -F "," 'NR>1 {print $4}' Expenses.csv | sort | uniq

Fetch transactions of a particular type to a separate file
sed -n '/Groceries/w Groceries.csv' Expenses.csv

Fetch max expenditure

BEGIN {FS=",";max = 0; } 
NR > 1 {if ($3<max) {max=$3; maxcat=$4; maxdate=$1}} 
END {print "The highest expenditure was", -max, "in", maxcat,"category on", maxdate}

Fetch expenditure on Merchandise
BEGIN {FS=","; tot=0}
NR > 1 {if($4 == "Merchandise" && $3 < 0 ) {tot += $3;}}
END{print "Total expenses on merchandise is", tot*-1}

Fetch expenses for a particular category
awk -v category="Groceries" -f CategoryExpenses2.awk Expenses.csv


Renaming a category
sed -n '/Other/s//Miscellaneous/' Expenses.csv



