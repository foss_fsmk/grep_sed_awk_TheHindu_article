BEGIN {FS=","; tot=0;cnt=0}
NR > 1 {if($4 == category && $3 < 0 ) {tot += $3; cnt += 1}}
END{
print "Total expenses on", category, "is", -tot;
print "There were", cnt, "transactions in this category";
}

