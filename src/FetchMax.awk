BEGIN {FS=",";max = 0; } 
NR > 1 {if ($3<max) {max=$3; maxcat=$4; maxdate=$1}} 
END {print "The highest expenditure was $", -max, "in", maxcat,"category on", maxdate}

